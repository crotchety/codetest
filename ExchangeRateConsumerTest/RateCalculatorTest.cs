﻿using System;
using ExchangeRateConsumer.Controller;
using ExchangeRateConsumer.Model;
using NUnit.Framework;

namespace ExchangeRateConsumerTest
{
    [TestFixture]
    public class RateCalculatorTest
    {
        ExchangeRateStore store = new ExchangeRateStore();
        public RateCalculatorTest()
        {
            store.baseCurrencyCode = "EUR";
            store.ExchangeRates.Add("GBP",(decimal)0.50);
            store.ExchangeRates.Add("EUR", 1);
            store.ExchangeRates.Add("USD",2);
        }
        [Test]
        public void calculatorRetrievesCorrectValues()
        {
            RateCalculator rateCalculator = new RateCalculator();
            var rate = rateCalculator.FindRate("EUR", "GBP", store);
            Assert.AreEqual(rate, .5);

            rate = rateCalculator.FindRate("GBP", "EUR", store);
            Assert.AreEqual(rate, 2);

            rate = rateCalculator.FindRate("USD", "GBP", store);
            Assert.AreEqual(rate, .25);

        }
    }
}
