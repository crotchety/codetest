﻿using System;
using ExchangeRateConsumer;
using ExchangeRateConsumer.Controller;
using ExchangeRateConsumer.Model;
using ExchangeRateConsumer.Service;
using ExchangeRateConsumer.View;
using NUnit.Framework;

namespace ExchangeRateConsumerTest
{
    [TestFixture]
    public class RateRequestControllerTest
    {
        MockResultDisplay mockResultDisplay = new MockResultDisplay();
        MockServiceConsumer serviceConsumer = new MockServiceConsumer();
        MockRateCalculator rateCalculator = new MockRateCalculator();
        [Test]
        public void RequestRunnerRequestTest()
        {
            RateRequestController requestController = new RateRequestController(serviceConsumer, mockResultDisplay,rateCalculator);


        }
    }


    public class MockResultDisplay : IResultDisplay
    {
        public void DisplayResult(string from, string to, decimal result)
        {
            throw new NotImplementedException();
        }
    }
    public class MockServiceConsumer : IServiceConsumer
    {
        public T FetchService<T>(string url) where T : class
        {
            throw new NotImplementedException();
        }
    }
    public class MockRateCalculator : IRateCalculator
    {
        public decimal FindRate(string from, string to, ExchangeRateStore rateStore)
        {
            throw new NotImplementedException();
        }
    }

}
