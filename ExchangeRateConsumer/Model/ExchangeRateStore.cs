﻿using System;
using System.Collections.Generic;

namespace ExchangeRateConsumer.Model
{
    public class ExchangeRateStore
    {
        private Dictionary<string, decimal> exchangeRates= new Dictionary<string, decimal>();

        public string baseCurrencyCode { get; set; }
        public Dictionary<string, decimal> ExchangeRates { get => exchangeRates; set => exchangeRates = value; }
    }
}
