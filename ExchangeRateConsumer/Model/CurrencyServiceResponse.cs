﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ExchangeRateConsumer.Model
{
    [Serializable()]
    public class CurrencyServiceResponse
    {
        public bool success { get; set; }
        public int timestamp { get; set; }
        [XmlElement(ElementName = "base")]
        public string baseCurrency { get; set; }
        public string date { get; set; }
        [XmlElement(ElementName = "rates")]
        public Dictionary<String,String> Rates { get; set; }

        public CurrencyServiceResponse()
        {
        }

    }
}
