﻿using System;
using System.Configuration;
using ExchangeRateConsumer.Controller;
using ExchangeRateConsumer.Service;
using ExchangeRateConsumer.View;

namespace ExchangeRateConsumer
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Getting exchange rates");

            getExchangeRate("EUR", "USD");
            getExchangeRate("EUR", "GBP");
            getExchangeRate("GBP", "USD");

        }
        public static void getExchangeRate(string from, string to)
        {
            RateRequestController requestController = new RateRequestController(new RestServiceConsumer(), new ConsoleResultDisplay(), new RateCalculator());
            requestController.getExchangeRate(from, to);
        }
    }
}
