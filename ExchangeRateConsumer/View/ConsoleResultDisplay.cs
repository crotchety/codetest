﻿using System;
using System.Collections.Generic;

namespace ExchangeRateConsumer.View
{
    public class ConsoleResultDisplay:IResultDisplay
    {
        public void DisplayResult(string from, string to, decimal result)
        {
            string outputText = string.Format("From {0} to {1} is {2}", from,to,result);
            Console.WriteLine(outputText);
            
        }
    }
}
