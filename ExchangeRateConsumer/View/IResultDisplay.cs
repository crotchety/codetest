﻿using System;
using System.Collections.Generic;

namespace ExchangeRateConsumer.View
{
    public interface IResultDisplay
    {
        void DisplayResult(string from, string to, decimal result);
    }
}
