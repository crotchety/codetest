﻿using System;
using System.Threading.Tasks;

namespace ExchangeRateConsumer.Service
{
    public interface IServiceConsumer
    {
        T FetchService<T>(string url) where T : class;
    }
}
