﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ExchangeRateConsumer.Service
{
    public class RestServiceConsumer: IServiceConsumer
    {
        public T FetchService<T>(string url) where T : class
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Accept = "application/json";
            var response = webRequest.GetResponse();
            var streamReader = new StreamReader(response.GetResponseStream());
            var responseContent = streamReader.ReadToEnd().Trim();
            var jsonObject = JsonConvert.DeserializeObject<T>(responseContent);
            return jsonObject;
        }


    }
}



