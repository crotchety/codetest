﻿using System;
using System.Configuration;
using System.Linq;
using ExchangeRateConsumer.Controller;
using ExchangeRateConsumer.Model;
using ExchangeRateConsumer.Service;
using ExchangeRateConsumer.View;

namespace ExchangeRateConsumer
{
    public class RateRequestController
    {
        IServiceConsumer serviceConsumer;
        IResultDisplay resultDisplay;
        IRateCalculator rateCalculator;

        public RateRequestController(IServiceConsumer serviceConsumer, IResultDisplay resultDisplay, IRateCalculator rateCalculator)
        {
            this.serviceConsumer = serviceConsumer;
            this.resultDisplay = resultDisplay;
            this.rateCalculator = rateCalculator;
        }
        internal void getExchangeRate(string from, string to)
        {
            CurrencyServiceResponse response = CallService();
            ExchangeRateStore rateStore = InitialiseRateStore(response);
            decimal exchangeRate = rateCalculator.FindRate(from, to, rateStore);
            resultDisplay.DisplayResult(from, to, exchangeRate);
        }

        private CurrencyServiceResponse CallService()
        {
            string url = ConfigurationManager.AppSettings.Get("serviceUrl");
            CurrencyServiceResponse response = serviceConsumer.FetchService<CurrencyServiceResponse>(url);
            return response;
        }

        private static ExchangeRateStore InitialiseRateStore(CurrencyServiceResponse response)
        {
            ExchangeRateStore rateStore = new ExchangeRateStore();
            rateStore.baseCurrencyCode = response.baseCurrency;
            rateStore.ExchangeRates = response.Rates.ToDictionary(item => item.Key, item => Decimal.Parse(item.Value, System.Globalization.NumberStyles.Float));
            return rateStore;
        }
    }
}
