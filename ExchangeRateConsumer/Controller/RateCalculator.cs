﻿using System;
using ExchangeRateConsumer.Model;

namespace ExchangeRateConsumer.Controller
{
    public class RateCalculator:IRateCalculator
    {
        ExchangeRateStore rateStore;
        decimal rateFrom;
        decimal rateTo;

        public decimal FindRate(string from, string to, ExchangeRateStore rateStore)
        {
            this.rateStore = rateStore;
            return calculateRequestedRate(to, from);
        }

        private decimal calculateRequestedRate(string to,string from)
        {
            rateTo = rateStore.ExchangeRates[to];
            rateFrom = rateStore.ExchangeRates[from];
            if (to.Equals(rateStore.baseCurrencyCode))
                return 1 / rateFrom;
            if (to.Equals(rateStore.baseCurrencyCode))
                return 1 / rateTo;
            else
                return rateTo/rateFrom;
        }


    }
}
