﻿using System;
using ExchangeRateConsumer.Model;

namespace ExchangeRateConsumer.Controller
{
    public interface IRateCalculator
    {
        decimal FindRate(string from, string to, ExchangeRateStore rateStore);
    }
}
